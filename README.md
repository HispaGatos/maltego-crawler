# maltego-crawler

A "crawler" transformer to add to our hacking tools/options. and just for fun.


![Alt text](/maltego-crawler.png?raw=true "Maltego with crawler transform in action")


---

_what do you need?_

Obviosly:
* Maltego
* Go
---

### Instalation

```
git clone https://gitlab.com/rek2/maltego-crawler.git
cd maltego-crawler
go mod init https://gitlab.com/rek2/maltego-crawler.git
go build -o maltego-crawler
cp maltego-crawler ~/bin/maltego-crawler #or anyplace you want this binary to be
```

- Add the OCR-A_char_Solidus.svg to Maltego
- Open Maltego and IMPORT the path-identity-to-import.mtz
- Open Maltego and add the transform, see below for a link to documentation

- Remember where you put the maltego-crawler resulting binary you will need that path/localtion for adding a local transform to maltego
- You adding to maltego a binary not a script so will be someththing line "/home/myuser/bin/maltego-crawler" instead of a scripting language interpreter.

[Upstream instructions in how to install a GO transform, but read my comment above!!!i](https://github.com/NoobieDog/maltegolocal/blob/master/README.md)